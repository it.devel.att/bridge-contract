// Code generated - DO NOT EDIT.
// This file is a generated binding and any manual changes will be lost.

package bridge

import (
	"errors"
	"math/big"
	"strings"

	ethereum "github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/event"
)

// Reference imports to suppress errors if they are not otherwise used.
var (
	_ = errors.New
	_ = big.NewInt
	_ = strings.NewReader
	_ = ethereum.NotFound
	_ = bind.Bind
	_ = common.Big1
	_ = types.BloomLookup
	_ = event.NewSubscription
)

// BridgeMetaData contains all meta data concerning the Bridge contract.
var BridgeMetaData = &bind.MetaData{
	ABI: "[{\"inputs\":[{\"internalType\":\"address\",\"name\":\"erc20Token\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"backendSigner\",\"type\":\"address\"}],\"stateMutability\":\"nonpayable\",\"type\":\"constructor\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_from\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_to\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"}],\"name\":\"RedeemComplete\",\"type\":\"event\"},{\"anonymous\":false,\"inputs\":[{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_from\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"address\",\"name\":\"_to\",\"type\":\"address\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"amount\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"uint256\",\"name\":\"_nonce\",\"type\":\"uint256\"},{\"indexed\":false,\"internalType\":\"bytes\",\"name\":\"_signature\",\"type\":\"bytes\"}],\"name\":\"SwapInitialized\",\"type\":\"event\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_from\",\"type\":\"address\"},{\"internalType\":\"address\",\"name\":\"_to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"},{\"internalType\":\"uint256\",\"name\":\"_nonce\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"_senderSignature\",\"type\":\"bytes\"},{\"internalType\":\"bytes\",\"name\":\"_backendSignature\",\"type\":\"bytes\"}],\"name\":\"redeem\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"inputs\":[{\"internalType\":\"address\",\"name\":\"_to\",\"type\":\"address\"},{\"internalType\":\"uint256\",\"name\":\"_amount\",\"type\":\"uint256\"},{\"internalType\":\"bytes\",\"name\":\"_signature\",\"type\":\"bytes\"}],\"name\":\"swap\",\"outputs\":[],\"stateMutability\":\"nonpayable\",\"type\":\"function\"}]",
	Bin: "0x608060405234801561001057600080fd5b5060405161094838038061094883398101604081905261002f9161007c565b600080546001600160a01b039384166001600160a01b031991821617909155600180549290931691161790556100ae565b80516001600160a01b038116811461007757600080fd5b919050565b6000806040838503121561008e578182fd5b61009783610060565b91506100a560208401610060565b90509250929050565b61088b806100bd6000396000f3fe608060405234801561001057600080fd5b50600436106100365760003560e01c806339aca1c11461003b5780639c48008814610050575b600080fd5b61004e610049366004610735565b610063565b005b61004e61005e366004610694565b61032a565b600083116100b85760405162461bcd60e51b815260206004820152601d60248201527f416d6f756e74206d7573742062652067726561746572207468616e203000000060448201526064015b60405180910390fd5b6001600160a01b03841661010e5760405162461bcd60e51b815260206004820152601a60248201527f43616e27742073656e6420746f207a65726f206164647265737300000000000060448201526064016100af565b6040516bffffffffffffffffffffffff19606086901b1660208201526034810184905260009061019c906054015b60408051601f1981840301815282825280516020918201207f19457468657265756d205369676e6564204d6573736167653a0a33320000000084830152603c8085019190915282518085039091018152605c909301909152815191012090565b9050336001600160a01b03166101e88285858080601f01602080910402602001604051908101604052809392919081815260200183838082843760009201919091525061054792505050565b6001600160a01b0316146102505760405162461bcd60e51b815260206004820152602960248201527f53656e646572206d757374207369676e206d65737361676520776974682070726044820152686976617465206b657960b81b60648201526084016100af565b33600090815260026020526040812080549161026b8361082e565b9091555050600054604051632770a7eb60e21b8152336004820152602481018690526001600160a01b0390911690639dc29fac90604401600060405180830381600087803b1580156102bc57600080fd5b505af11580156102d0573d6000803e3d6000fd5b505033600081815260026020526040908190205490517fa1bdba5f25ad9935238e2d977db16127039649b3142722f5b6bbb546404e6c66945061031b935089918991899089906107d5565b60405180910390a15050505050565b6001600160a01b038816600090815260036020908152604080832088845290915290205460ff161561039e5760405162461bcd60e51b815260206004820152601a60248201527f7472616e7366657220616c72656164792070726f63657373656400000000000060448201526064016100af565b60006103be89898989898960405160200161013c9695949392919061078d565b600154604080516020601f87018190048102820181019092528581529293506001600160a01b039091169161041091849190879087908190840183828082843760009201919091525061054792505050565b6001600160a01b0316146104665760405162461bcd60e51b815260206004820152601760248201527f77726f6e67206261636b656e64207369676e617475726500000000000000000060448201526064016100af565b336000908152600360209081526040808320898452909152808220805460ff19166001179055905490516340c10f1960e01b81526001600160a01b038a81166004830152602482018a9052909116906340c10f1990604401600060405180830381600087803b1580156104d857600080fd5b505af11580156104ec573d6000803e3d6000fd5b5050604080516001600160a01b03808e1682528c1660208201529081018a90527fe1454cdbf1606501a48571d6928126338d58e24d7c897787e614576009b5d4239250606001905060405180910390a1505050505050505050565b600080600080610556856105c6565b6040805160008152602081018083528b905260ff8516918101919091526060810183905260808101829052929550909350915060019060a0016020604051602081039080840390855afa1580156105b1573d6000803e3d6000fd5b5050604051601f190151979650505050505050565b600080600083516041146106155760405162461bcd60e51b81526020600482015260166024820152750eee4dedcce40e6d2cedcc2e8eae4ca40d8cadccee8d60531b60448201526064016100af565b5050506020810151604082015160609092015160001a92909190565b80356001600160a01b038116811461064857600080fd5b919050565b60008083601f84011261065e578182fd5b50813567ffffffffffffffff811115610675578182fd5b60208301915083602082850101111561068d57600080fd5b9250929050565b60008060008060008060008060c0898b0312156106af578384fd5b6106b889610631565b97506106c660208a01610631565b96506040890135955060608901359450608089013567ffffffffffffffff808211156106f0578586fd5b6106fc8c838d0161064d565b909650945060a08b0135915080821115610714578384fd5b506107218b828c0161064d565b999c989b5096995094979396929594505050565b6000806000806060858703121561074a578384fd5b61075385610631565b935060208501359250604085013567ffffffffffffffff811115610775578283fd5b6107818782880161064d565b95989497509550505050565b60006bffffffffffffffffffffffff19808960601b168352808860601b1660148401525085602883015284604883015282846068840137910160680190815295945050505050565b6001600160a01b03878116825286166020820152604081018590526060810184905260a06080820181905281018290526000828460c084013781830160c090810191909152601f909201601f1916010195945050505050565b600060001982141561084e57634e487b7160e01b81526011600452602481fd5b506001019056fea2646970667358221220ec32f3bb6aa1706c5474f9a93098e15639d40b94c436f7774827922727ba815264736f6c63430008040033",
}

// BridgeABI is the input ABI used to generate the binding from.
// Deprecated: Use BridgeMetaData.ABI instead.
var BridgeABI = BridgeMetaData.ABI

// BridgeBin is the compiled bytecode used for deploying new contracts.
// Deprecated: Use BridgeMetaData.Bin instead.
var BridgeBin = BridgeMetaData.Bin

// DeployBridge deploys a new Ethereum contract, binding an instance of Bridge to it.
func DeployBridge(auth *bind.TransactOpts, backend bind.ContractBackend, erc20Token common.Address, backendSigner common.Address) (common.Address, *types.Transaction, *Bridge, error) {
	parsed, err := BridgeMetaData.GetAbi()
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	if parsed == nil {
		return common.Address{}, nil, nil, errors.New("GetABI returned nil")
	}

	address, tx, contract, err := bind.DeployContract(auth, *parsed, common.FromHex(BridgeBin), backend, erc20Token, backendSigner)
	if err != nil {
		return common.Address{}, nil, nil, err
	}
	return address, tx, &Bridge{BridgeCaller: BridgeCaller{contract: contract}, BridgeTransactor: BridgeTransactor{contract: contract}, BridgeFilterer: BridgeFilterer{contract: contract}}, nil
}

// Bridge is an auto generated Go binding around an Ethereum contract.
type Bridge struct {
	BridgeCaller     // Read-only binding to the contract
	BridgeTransactor // Write-only binding to the contract
	BridgeFilterer   // Log filterer for contract events
}

// BridgeCaller is an auto generated read-only Go binding around an Ethereum contract.
type BridgeCaller struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// BridgeTransactor is an auto generated write-only Go binding around an Ethereum contract.
type BridgeTransactor struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// BridgeFilterer is an auto generated log filtering Go binding around an Ethereum contract events.
type BridgeFilterer struct {
	contract *bind.BoundContract // Generic contract wrapper for the low level calls
}

// BridgeSession is an auto generated Go binding around an Ethereum contract,
// with pre-set call and transact options.
type BridgeSession struct {
	Contract     *Bridge           // Generic contract binding to set the session for
	CallOpts     bind.CallOpts     // Call options to use throughout this session
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// BridgeCallerSession is an auto generated read-only Go binding around an Ethereum contract,
// with pre-set call options.
type BridgeCallerSession struct {
	Contract *BridgeCaller // Generic contract caller binding to set the session for
	CallOpts bind.CallOpts // Call options to use throughout this session
}

// BridgeTransactorSession is an auto generated write-only Go binding around an Ethereum contract,
// with pre-set transact options.
type BridgeTransactorSession struct {
	Contract     *BridgeTransactor // Generic contract transactor binding to set the session for
	TransactOpts bind.TransactOpts // Transaction auth options to use throughout this session
}

// BridgeRaw is an auto generated low-level Go binding around an Ethereum contract.
type BridgeRaw struct {
	Contract *Bridge // Generic contract binding to access the raw methods on
}

// BridgeCallerRaw is an auto generated low-level read-only Go binding around an Ethereum contract.
type BridgeCallerRaw struct {
	Contract *BridgeCaller // Generic read-only contract binding to access the raw methods on
}

// BridgeTransactorRaw is an auto generated low-level write-only Go binding around an Ethereum contract.
type BridgeTransactorRaw struct {
	Contract *BridgeTransactor // Generic write-only contract binding to access the raw methods on
}

// NewBridge creates a new instance of Bridge, bound to a specific deployed contract.
func NewBridge(address common.Address, backend bind.ContractBackend) (*Bridge, error) {
	contract, err := bindBridge(address, backend, backend, backend)
	if err != nil {
		return nil, err
	}
	return &Bridge{BridgeCaller: BridgeCaller{contract: contract}, BridgeTransactor: BridgeTransactor{contract: contract}, BridgeFilterer: BridgeFilterer{contract: contract}}, nil
}

// NewBridgeCaller creates a new read-only instance of Bridge, bound to a specific deployed contract.
func NewBridgeCaller(address common.Address, caller bind.ContractCaller) (*BridgeCaller, error) {
	contract, err := bindBridge(address, caller, nil, nil)
	if err != nil {
		return nil, err
	}
	return &BridgeCaller{contract: contract}, nil
}

// NewBridgeTransactor creates a new write-only instance of Bridge, bound to a specific deployed contract.
func NewBridgeTransactor(address common.Address, transactor bind.ContractTransactor) (*BridgeTransactor, error) {
	contract, err := bindBridge(address, nil, transactor, nil)
	if err != nil {
		return nil, err
	}
	return &BridgeTransactor{contract: contract}, nil
}

// NewBridgeFilterer creates a new log filterer instance of Bridge, bound to a specific deployed contract.
func NewBridgeFilterer(address common.Address, filterer bind.ContractFilterer) (*BridgeFilterer, error) {
	contract, err := bindBridge(address, nil, nil, filterer)
	if err != nil {
		return nil, err
	}
	return &BridgeFilterer{contract: contract}, nil
}

// bindBridge binds a generic wrapper to an already deployed contract.
func bindBridge(address common.Address, caller bind.ContractCaller, transactor bind.ContractTransactor, filterer bind.ContractFilterer) (*bind.BoundContract, error) {
	parsed, err := abi.JSON(strings.NewReader(BridgeABI))
	if err != nil {
		return nil, err
	}
	return bind.NewBoundContract(address, parsed, caller, transactor, filterer), nil
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Bridge *BridgeRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Bridge.Contract.BridgeCaller.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Bridge *BridgeRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Bridge.Contract.BridgeTransactor.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Bridge *BridgeRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Bridge.Contract.BridgeTransactor.contract.Transact(opts, method, params...)
}

// Call invokes the (constant) contract method with params as input values and
// sets the output to result. The result type might be a single field for simple
// returns, a slice of interfaces for anonymous returns and a struct for named
// returns.
func (_Bridge *BridgeCallerRaw) Call(opts *bind.CallOpts, result *[]interface{}, method string, params ...interface{}) error {
	return _Bridge.Contract.contract.Call(opts, result, method, params...)
}

// Transfer initiates a plain transaction to move funds to the contract, calling
// its default method if one is available.
func (_Bridge *BridgeTransactorRaw) Transfer(opts *bind.TransactOpts) (*types.Transaction, error) {
	return _Bridge.Contract.contract.Transfer(opts)
}

// Transact invokes the (paid) contract method with params as input values.
func (_Bridge *BridgeTransactorRaw) Transact(opts *bind.TransactOpts, method string, params ...interface{}) (*types.Transaction, error) {
	return _Bridge.Contract.contract.Transact(opts, method, params...)
}

// Redeem is a paid mutator transaction binding the contract method 0x9c480088.
//
// Solidity: function redeem(address _from, address _to, uint256 _amount, uint256 _nonce, bytes _senderSignature, bytes _backendSignature) returns()
func (_Bridge *BridgeTransactor) Redeem(opts *bind.TransactOpts, _from common.Address, _to common.Address, _amount *big.Int, _nonce *big.Int, _senderSignature []byte, _backendSignature []byte) (*types.Transaction, error) {
	return _Bridge.contract.Transact(opts, "redeem", _from, _to, _amount, _nonce, _senderSignature, _backendSignature)
}

// Redeem is a paid mutator transaction binding the contract method 0x9c480088.
//
// Solidity: function redeem(address _from, address _to, uint256 _amount, uint256 _nonce, bytes _senderSignature, bytes _backendSignature) returns()
func (_Bridge *BridgeSession) Redeem(_from common.Address, _to common.Address, _amount *big.Int, _nonce *big.Int, _senderSignature []byte, _backendSignature []byte) (*types.Transaction, error) {
	return _Bridge.Contract.Redeem(&_Bridge.TransactOpts, _from, _to, _amount, _nonce, _senderSignature, _backendSignature)
}

// Redeem is a paid mutator transaction binding the contract method 0x9c480088.
//
// Solidity: function redeem(address _from, address _to, uint256 _amount, uint256 _nonce, bytes _senderSignature, bytes _backendSignature) returns()
func (_Bridge *BridgeTransactorSession) Redeem(_from common.Address, _to common.Address, _amount *big.Int, _nonce *big.Int, _senderSignature []byte, _backendSignature []byte) (*types.Transaction, error) {
	return _Bridge.Contract.Redeem(&_Bridge.TransactOpts, _from, _to, _amount, _nonce, _senderSignature, _backendSignature)
}

// Swap is a paid mutator transaction binding the contract method 0x39aca1c1.
//
// Solidity: function swap(address _to, uint256 _amount, bytes _signature) returns()
func (_Bridge *BridgeTransactor) Swap(opts *bind.TransactOpts, _to common.Address, _amount *big.Int, _signature []byte) (*types.Transaction, error) {
	return _Bridge.contract.Transact(opts, "swap", _to, _amount, _signature)
}

// Swap is a paid mutator transaction binding the contract method 0x39aca1c1.
//
// Solidity: function swap(address _to, uint256 _amount, bytes _signature) returns()
func (_Bridge *BridgeSession) Swap(_to common.Address, _amount *big.Int, _signature []byte) (*types.Transaction, error) {
	return _Bridge.Contract.Swap(&_Bridge.TransactOpts, _to, _amount, _signature)
}

// Swap is a paid mutator transaction binding the contract method 0x39aca1c1.
//
// Solidity: function swap(address _to, uint256 _amount, bytes _signature) returns()
func (_Bridge *BridgeTransactorSession) Swap(_to common.Address, _amount *big.Int, _signature []byte) (*types.Transaction, error) {
	return _Bridge.Contract.Swap(&_Bridge.TransactOpts, _to, _amount, _signature)
}

// BridgeRedeemCompleteIterator is returned from FilterRedeemComplete and is used to iterate over the raw logs and unpacked data for RedeemComplete events raised by the Bridge contract.
type BridgeRedeemCompleteIterator struct {
	Event *BridgeRedeemComplete // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *BridgeRedeemCompleteIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(BridgeRedeemComplete)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(BridgeRedeemComplete)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *BridgeRedeemCompleteIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *BridgeRedeemCompleteIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// BridgeRedeemComplete represents a RedeemComplete event raised by the Bridge contract.
type BridgeRedeemComplete struct {
	From   common.Address
	To     common.Address
	Amount *big.Int
	Raw    types.Log // Blockchain specific contextual infos
}

// FilterRedeemComplete is a free log retrieval operation binding the contract event 0xe1454cdbf1606501a48571d6928126338d58e24d7c897787e614576009b5d423.
//
// Solidity: event RedeemComplete(address _from, address _to, uint256 amount)
func (_Bridge *BridgeFilterer) FilterRedeemComplete(opts *bind.FilterOpts) (*BridgeRedeemCompleteIterator, error) {

	logs, sub, err := _Bridge.contract.FilterLogs(opts, "RedeemComplete")
	if err != nil {
		return nil, err
	}
	return &BridgeRedeemCompleteIterator{contract: _Bridge.contract, event: "RedeemComplete", logs: logs, sub: sub}, nil
}

// WatchRedeemComplete is a free log subscription operation binding the contract event 0xe1454cdbf1606501a48571d6928126338d58e24d7c897787e614576009b5d423.
//
// Solidity: event RedeemComplete(address _from, address _to, uint256 amount)
func (_Bridge *BridgeFilterer) WatchRedeemComplete(opts *bind.WatchOpts, sink chan<- *BridgeRedeemComplete) (event.Subscription, error) {

	logs, sub, err := _Bridge.contract.WatchLogs(opts, "RedeemComplete")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(BridgeRedeemComplete)
				if err := _Bridge.contract.UnpackLog(event, "RedeemComplete", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseRedeemComplete is a log parse operation binding the contract event 0xe1454cdbf1606501a48571d6928126338d58e24d7c897787e614576009b5d423.
//
// Solidity: event RedeemComplete(address _from, address _to, uint256 amount)
func (_Bridge *BridgeFilterer) ParseRedeemComplete(log types.Log) (*BridgeRedeemComplete, error) {
	event := new(BridgeRedeemComplete)
	if err := _Bridge.contract.UnpackLog(event, "RedeemComplete", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}

// BridgeSwapInitializedIterator is returned from FilterSwapInitialized and is used to iterate over the raw logs and unpacked data for SwapInitialized events raised by the Bridge contract.
type BridgeSwapInitializedIterator struct {
	Event *BridgeSwapInitialized // Event containing the contract specifics and raw log

	contract *bind.BoundContract // Generic contract to use for unpacking event data
	event    string              // Event name to use for unpacking event data

	logs chan types.Log        // Log channel receiving the found contract events
	sub  ethereum.Subscription // Subscription for errors, completion and termination
	done bool                  // Whether the subscription completed delivering logs
	fail error                 // Occurred error to stop iteration
}

// Next advances the iterator to the subsequent event, returning whether there
// are any more events found. In case of a retrieval or parsing error, false is
// returned and Error() can be queried for the exact failure.
func (it *BridgeSwapInitializedIterator) Next() bool {
	// If the iterator failed, stop iterating
	if it.fail != nil {
		return false
	}
	// If the iterator completed, deliver directly whatever's available
	if it.done {
		select {
		case log := <-it.logs:
			it.Event = new(BridgeSwapInitialized)
			if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
				it.fail = err
				return false
			}
			it.Event.Raw = log
			return true

		default:
			return false
		}
	}
	// Iterator still in progress, wait for either a data or an error event
	select {
	case log := <-it.logs:
		it.Event = new(BridgeSwapInitialized)
		if err := it.contract.UnpackLog(it.Event, it.event, log); err != nil {
			it.fail = err
			return false
		}
		it.Event.Raw = log
		return true

	case err := <-it.sub.Err():
		it.done = true
		it.fail = err
		return it.Next()
	}
}

// Error returns any retrieval or parsing error occurred during filtering.
func (it *BridgeSwapInitializedIterator) Error() error {
	return it.fail
}

// Close terminates the iteration process, releasing any pending underlying
// resources.
func (it *BridgeSwapInitializedIterator) Close() error {
	it.sub.Unsubscribe()
	return nil
}

// BridgeSwapInitialized represents a SwapInitialized event raised by the Bridge contract.
type BridgeSwapInitialized struct {
	From      common.Address
	To        common.Address
	Amount    *big.Int
	Nonce     *big.Int
	Signature []byte
	Raw       types.Log // Blockchain specific contextual infos
}

// FilterSwapInitialized is a free log retrieval operation binding the contract event 0xa1bdba5f25ad9935238e2d977db16127039649b3142722f5b6bbb546404e6c66.
//
// Solidity: event SwapInitialized(address _from, address _to, uint256 amount, uint256 _nonce, bytes _signature)
func (_Bridge *BridgeFilterer) FilterSwapInitialized(opts *bind.FilterOpts) (*BridgeSwapInitializedIterator, error) {

	logs, sub, err := _Bridge.contract.FilterLogs(opts, "SwapInitialized")
	if err != nil {
		return nil, err
	}
	return &BridgeSwapInitializedIterator{contract: _Bridge.contract, event: "SwapInitialized", logs: logs, sub: sub}, nil
}

// WatchSwapInitialized is a free log subscription operation binding the contract event 0xa1bdba5f25ad9935238e2d977db16127039649b3142722f5b6bbb546404e6c66.
//
// Solidity: event SwapInitialized(address _from, address _to, uint256 amount, uint256 _nonce, bytes _signature)
func (_Bridge *BridgeFilterer) WatchSwapInitialized(opts *bind.WatchOpts, sink chan<- *BridgeSwapInitialized) (event.Subscription, error) {

	logs, sub, err := _Bridge.contract.WatchLogs(opts, "SwapInitialized")
	if err != nil {
		return nil, err
	}
	return event.NewSubscription(func(quit <-chan struct{}) error {
		defer sub.Unsubscribe()
		for {
			select {
			case log := <-logs:
				// New log arrived, parse the event and forward to the user
				event := new(BridgeSwapInitialized)
				if err := _Bridge.contract.UnpackLog(event, "SwapInitialized", log); err != nil {
					return err
				}
				event.Raw = log

				select {
				case sink <- event:
				case err := <-sub.Err():
					return err
				case <-quit:
					return nil
				}
			case err := <-sub.Err():
				return err
			case <-quit:
				return nil
			}
		}
	}), nil
}

// ParseSwapInitialized is a log parse operation binding the contract event 0xa1bdba5f25ad9935238e2d977db16127039649b3142722f5b6bbb546404e6c66.
//
// Solidity: event SwapInitialized(address _from, address _to, uint256 amount, uint256 _nonce, bytes _signature)
func (_Bridge *BridgeFilterer) ParseSwapInitialized(log types.Log) (*BridgeSwapInitialized, error) {
	event := new(BridgeSwapInitialized)
	if err := _Bridge.contract.UnpackLog(event, "SwapInitialized", log); err != nil {
		return nil, err
	}
	event.Raw = log
	return event, nil
}
