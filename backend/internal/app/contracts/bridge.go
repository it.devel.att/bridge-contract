package contracts

import (
	"math/big"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/ethereum/go-ethereum/event"

	"eth-bridge/internal/app/configs"
	"eth-bridge/internal/app/contracts/generated/bridge"
)

type BridgeContract interface {
	Swap(opts *bind.TransactOpts, _to common.Address, _amount *big.Int, _signature []byte) (*types.Transaction, error)
	SubscribeSwapInitializedEvent(opts *bind.WatchOpts, events chan *bridge.BridgeSwapInitialized) (event.Subscription, error)
}

type Bridge struct {
	contract *bridge.Bridge
}

func NewBridgeContract(cfg configs.Bridge, client *ethclient.Client) (*Bridge, error) {
	contract, err := bridge.NewBridge(common.HexToAddress(cfg.Address), client)
	if err != nil {
		return nil, err
	}
	return &Bridge{contract: contract}, nil
}

func (b *Bridge) Swap(
	opts *bind.TransactOpts,
	_to common.Address,
	_amount *big.Int,
	_signature []byte,
) (*types.Transaction, error) {
	// TODO Err metrics and logs
	return b.contract.Swap(opts, _to, _amount, _signature)
}

func (b *Bridge) Redeem(
	opts *bind.TransactOpts,
	_from common.Address,
	_to common.Address,
	_amount *big.Int,
	_nonce *big.Int,
	_signature []byte,
	_backendSignature []byte,
) (*types.Transaction, error) {
	// TODO Err metrics and logs
	return b.contract.Redeem(opts, _from, _to, _amount, _nonce, _signature, _backendSignature)
}

func (b *Bridge) SubscribeSwapInitializedEvent(opts *bind.WatchOpts, events chan *bridge.BridgeSwapInitialized) (event.Subscription, error) {
	return b.contract.WatchSwapInitialized(opts, events)
}
