package signers

import (
	"encoding/hex"
	"math/big"
	"testing"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/common/math"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/stretchr/testify/require"
	"gotest.tools/assert"
)

//address     0x5E642f8C02Fe41991Db336d1425Ad0F58f3019F8
//privateKey  0x65302126d4687ba2d075f46167c6d4e107bb1e08fc8e6e8ef5b151effb48dad4
//publicKey   0x040a2c6d665cc358d308ff74da79d0f5d5f926d686a01ce1fa94e16418595198541a68aa24c28b04d75dfd9dbb0a548c03e7dfc63010c3482a36d17092af171558
const privateTestKey = "65302126d4687ba2d075f46167c6d4e107bb1e08fc8e6e8ef5b151effb48dad4"

func TestBackendSigner_Sign(t *testing.T) {
	backendSigner, err := NewBackendSigner(privateTestKey)
	require.NoError(t, err)

	fromAddress := common.HexToAddress("0xf39Fd6e51aad88F6F4ce6aB8827279cffFb92266")
	toAddress := common.HexToAddress("0x90F79bf6EB2c4f870365E785982E1f101E93b906")
	am := new(big.Int)
	am.SetString("10000000000000000000", 10)

	amount := math.U256Bytes(am)

	no := new(big.Int)
	no.SetString("1", 10)
	nonce := math.U256Bytes(no)

	signerSignature, err := hex.DecodeString("e2128c1f831d01a2630c2bdb1264528038fcbea01355821c2c600bd870ed3ccb6f4981228888a518c1a5462a52edccb42637f18f10ea6317ebede18b4747594e1c")
	require.NoError(t, err)

	backendMessage := crypto.Keccak256Hash(
		fromAddress.Bytes(),
		toAddress.Bytes(),
		amount,
		nonce,
		signerSignature,
	)
	backendSignerResult, err := backendSigner.Sign(backendMessage.Bytes())
	require.NoError(t, err)

	assert.Equal(t, "0x7e0951bdb209663108f21e38ad79bc1ee8376d15a274f8bc3834597eca3f972c", backendMessage.String())
	assert.Equal(t, "0x36413e28f74707c64a1757198e8af2aeb42e72a4fdb5ddeaaa4ae084918f244c1bdb8cb4be94facff80a34fe18c4a126c4a6fe030ff6197613d4d692b21eb9db1b", backendSignerResult)

}
