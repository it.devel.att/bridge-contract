package signers

import (
	"crypto/ecdsa"
	"fmt"
	"strings"

	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
)

type Signer interface {
	Sign(data []byte) (string, error)
}

type BackendSigner struct {
	privateKey *ecdsa.PrivateKey
}

var _ Signer = (*BackendSigner)(nil)

func NewBackendSigner(pkey string) (*BackendSigner, error) {
	pk, err := UnmarshallHEXPrivateKey(pkey)
	if err != nil {
		return nil, err
	}
	return &BackendSigner{privateKey: pk}, nil

}

func (s *BackendSigner) Sign(hash []byte) (string, error) {
	hash = crypto.Keccak256Hash(
		[]byte(fmt.Sprintf("\x19Ethereum Signed Message:\n%v", len(hash))),
		hash,
	).Bytes()
	result, err := crypto.Sign(hash, s.privateKey)
	if err != nil {
		return "", err
	}
	if result[64] == 0 || result[64] == 1 {
		result[64] += 27
	}
	hexString := common.Bytes2Hex(result)
	if !strings.HasPrefix(hexString, "0x") {
		hexString = "0x" + hexString
	}
	return hexString, nil
}

func UnmarshallHEXPrivateKey(privKeyHex string) (*ecdsa.PrivateKey, error) {
	return crypto.HexToECDSA(privKeyHex)
}
