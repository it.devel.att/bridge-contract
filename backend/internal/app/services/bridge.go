package services

import (
	"context"
	"encoding/hex"
	"sync"

	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common/math"
	"github.com/ethereum/go-ethereum/crypto"
	"go.uber.org/zap"

	"eth-bridge/internal/app/contracts"
	"eth-bridge/internal/app/contracts/generated/bridge"
	"eth-bridge/internal/app/repositories"
	"eth-bridge/internal/app/repositories/models"
	"eth-bridge/internal/app/signers"
)

type SwapEventService interface {
	ListenSwapEvents() error
	Stop() error
}

type SEService struct {
	log *zap.Logger
	ctx context.Context
	wg  *sync.WaitGroup

	contract            contracts.BridgeContract
	swapEventRepository repositories.SwapEventRepository
	backendSigner       signers.Signer
}

var _ SwapEventService = (*SEService)(nil)

func NewSwapEventService(
	ctx context.Context,
	log *zap.Logger,
	contract contracts.BridgeContract,
	repo repositories.SwapEventRepository,
	backendSigner signers.Signer,
) *SEService {
	return &SEService{
		log:                 log.Named("SwapEventService"),
		ctx:                 ctx,
		wg:                  &sync.WaitGroup{},
		contract:            contract,
		swapEventRepository: repo,
		backendSigner:       backendSigner,
	}
}

func (l *SEService) ListenSwapEvents() error {
	events := make(chan *bridge.BridgeSwapInitialized)
	subs, err := l.contract.SubscribeSwapInitializedEvent(&bind.WatchOpts{}, events)
	if err != nil {
		return err
	}
	l.wg.Add(1)
	go func() {
		defer l.wg.Done()

		for {
			select {
			case <-l.ctx.Done():
				l.log.Info("Receive done signal")
				return
			case e := <-subs.Err():
				// TODO Retries or panic logic
				l.log.Error("Err", zap.Error(e))
				return
			case event := <-events:
				if err := l.handleEvent(event); err != nil {
					l.log.Error("error while try to handle event", zap.Error(err))
				}
			}
		}
	}()
	return nil
}

func (l *SEService) handleEvent(event *bridge.BridgeSwapInitialized) error {
	dbEvent := &models.SwapEvent{
		From:      event.From.String(),
		To:        event.To.String(),
		Amount:    event.Amount.String(),
		Nonce:     event.Nonce.String(),
		Signature: hex.EncodeToString(event.Signature),
	}
	h := crypto.Keccak256Hash(
		event.From.Bytes(),
		event.To.Bytes(),
		math.U256Bytes(event.Amount),
		math.U256Bytes(event.Nonce),
		event.Signature,
	)
	backendSignature, err := l.backendSigner.Sign(h.Bytes())
	if err != nil {
		return err
	}
	dbEvent.BackendSignature = backendSignature
	if err := l.swapEventRepository.Create(l.ctx, dbEvent); err != nil {
		return err
	}
	return nil
}

func (l *SEService) Stop() error {
	l.wg.Wait()
	return nil
}
