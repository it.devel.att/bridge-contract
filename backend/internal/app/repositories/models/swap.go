package models

type SwapEvent struct {
	From             string `json:"from"`
	To               string `json:"to"`
	Amount           string `json:"amount"`
	Nonce            string `json:"nonce"`
	Signature        string `json:"signature"`
	BackendSignature string `json:"backend_signature"`
}
