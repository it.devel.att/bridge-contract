package repositories

import (
	"context"

	"eth-bridge/internal/app/repositories/models"
)

type SwapEventRepository interface {
	Create(ctx context.Context, event *models.SwapEvent) error
	List(ctx context.Context) ([]*models.SwapEvent, error)
}
