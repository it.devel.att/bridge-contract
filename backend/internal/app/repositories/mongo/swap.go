package mongo

import (
	"context"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"eth-bridge/internal/app/db"
	"eth-bridge/internal/app/repositories"
	"eth-bridge/internal/app/repositories/models"
)

type SwapEventRepository struct {
	client     *db.MongoClient
	collection *mongo.Collection
}

var _ repositories.SwapEventRepository = (*SwapEventRepository)(nil)

func NewSwapEventRepository(client *db.MongoClient) *SwapEventRepository {
	collection := client.Database.Collection("swap_events")
	return &SwapEventRepository{client: client, collection: collection}
}

func (s *SwapEventRepository) Create(ctx context.Context, event *models.SwapEvent) error {
	fieldsSet := bson.D{
		{"from", event.From},
		{"to", event.To},
		{"amount", event.Amount},
		{"nonce", event.Nonce},
		{"signature", event.Signature},
		{"backend_signature", event.BackendSignature},
	}
	filter := fieldsSet
	update := bson.D{
		{"$set", fieldsSet},
	}
	opts := options.Update().SetUpsert(true)
	_, err := s.collection.UpdateOne(ctx, filter, update, opts)
	if err != nil {
		return err
	}
	return nil
}

func (s *SwapEventRepository) List(ctx context.Context) ([]*models.SwapEvent, error) {
	cursor, err := s.collection.Find(ctx, bson.M{})
	if err != nil {
		return nil, err
	}
	var events []*models.SwapEvent
	if err = cursor.All(ctx, &events); err != nil {
		return nil, err
	}
	return events, nil
}
