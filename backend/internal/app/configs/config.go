package configs

import (
	"strings"
	"time"

	"github.com/spf13/viper"
)

type Config struct {
	ETH       ETH       `mapstructure:"eth"`
	Contracts Contracts `mapstructure:"contracts"`
	Postgres  Postgres  `mapstructure:"postgres"`
	Mongo     Mongo     `mapstructure:"mongo"`
	Backend   Backend   `mapstructure:"backend"`
}

type ETH struct {
	ClientURL string `mapstructure:"client_url"`
}

type Contracts struct {
	Bridge Bridge `mapstructure:"bridge"`
}

type Bridge struct {
	Address string `mapstructure:"address"`
}

type Postgres struct {
	DSN string `mapstructure:"dsn"`
}

type Mongo struct {
	DSN                         string        `mapstructure:"dsn"`
	Database                    string        `mapstructure:"database"`
	DefaultConnectOrDropTimeout time.Duration `mapstructure:"default_connect_or_drop_timeout"`
}

type Backend struct {
	PrivateKey string `mapstructure:"private_key"`
}

func NewConfig() (*Config, error) {
	config := &Config{}

	viper.SetDefault("eth.client_url", "set_it")
	viper.SetDefault("contracts.bridge.address", "set_it")

	viper.SetDefault("postgres.dsn", "set_it")

	viper.SetDefault("mongo.dsn", "set_it")
	viper.SetDefault("mongo.database", "test_set_it")
	viper.SetDefault("mongo.default_connect_or_drop_timeout", time.Second*5)

	viper.SetDefault("backend.private_key", "set_it")

	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()

	err := viper.Unmarshal(config)
	return config, err
}
