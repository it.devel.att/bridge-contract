package db

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"

	"eth-bridge/internal/app/configs"
)

type MongoClient struct {
	Config   configs.Mongo
	Client   *mongo.Client
	Database *mongo.Database
}

func NewMongoClient(cfg configs.Mongo) (*MongoClient, error) {
	opt := options.Client()
	opt.ApplyURI(cfg.DSN)

	client, err := mongo.NewClient(opt)
	if err != nil {
		return nil, fmt.Errorf("[NewMongoClient] cannot create mongodb client: %v", err)
	}
	ctx, cancel := context.WithTimeout(context.Background(), cfg.DefaultConnectOrDropTimeout)
	defer cancel()
	err = client.Connect(ctx)
	if err != nil {
		return nil, fmt.Errorf("[NewMongoClient] cannot connect to mongodb at %s: %v", opt.GetURI(), err)

	}
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		return nil, fmt.Errorf("[NewMongoClient] mongodb ping at %s failed: %v", cfg.DSN, err)
	}
	db := client.Database(cfg.Database)
	return &MongoClient{
		Config:   cfg,
		Client:   client,
		Database: db,
	}, nil
}

func (c *MongoClient) Disconnect() {
	ctx, cancel := context.WithTimeout(context.Background(), c.Config.DefaultConnectOrDropTimeout)
	defer cancel()

	err := c.Client.Disconnect(ctx)
	if err != nil {
		return
	}
}
