package dependencies

import (
	"context"
	"os"
	"os/signal"
	"syscall"

	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/joho/godotenv"
	_ "github.com/joho/godotenv"
	"go.uber.org/zap"

	"eth-bridge/internal/app/configs"
	"eth-bridge/internal/app/contracts"
	"eth-bridge/internal/app/db"
	"eth-bridge/internal/app/repositories"
	"eth-bridge/internal/app/repositories/mongo"
	"eth-bridge/internal/app/services"
	"eth-bridge/internal/app/signers"
)

type Dependencies interface {
	Context() context.Context
	Config() *configs.Config
	ETHClient() *ethclient.Client
	BridgeContract() contracts.BridgeContract
	BackendSigner() signers.Signer
	SwapEventService() services.SwapEventService
	WaitForGracefulShutdown()
}

type Deps struct {
	ctx        context.Context
	cancelFunc context.CancelFunc
	log        *zap.Logger

	config *configs.Config

	mongoClient *db.MongoClient

	ethClient      *ethclient.Client
	bridgeContract contracts.BridgeContract

	swapEventRepo repositories.SwapEventRepository

	backendSigner signers.Signer

	swapEventService services.SwapEventService

	stopCallbacks []func() error
}

var _ = (*Deps)(nil)

func NewDependencies() Dependencies {
	logger, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}
	err = godotenv.Load()
	if err != nil {
		logger.Warn("Error reading env file: ", zap.Error(err))
	}

	ctx, cancel := context.WithCancel(context.Background())
	return &Deps{ctx: ctx, cancelFunc: cancel, log: logger.Named("dependencies")}
}

func (d *Deps) Context() context.Context {
	return d.ctx
}

func (d *Deps) Config() *configs.Config {
	if d.config == nil {
		var err error
		if d.config, err = configs.NewConfig(); err != nil {
			d.log.Panic("GetConfig", zap.Error(err))
		}
		d.log.Info("Success initialize config")
	}
	return d.config
}

func (d *Deps) MongoClient() *db.MongoClient {
	if d.mongoClient == nil {
		var err error
		if d.mongoClient, err = db.NewMongoClient(d.Config().Mongo); err != nil {
			d.log.Panic("NewMongoClient", zap.Error(err))
		}
		d.addStopCallback(wrapNilError(d.mongoClient.Disconnect))
		d.log.Info("Success initialize mongoClient")
	}
	return d.mongoClient
}

func (d *Deps) ETHClient() *ethclient.Client {
	if d.ethClient == nil {
		var err error
		if d.ethClient, err = ethclient.DialContext(d.ctx, d.Config().ETH.ClientURL); err != nil {
			d.log.Panic("ETHClient", zap.Error(err))
		}
		d.addStopCallback(wrapNilError(d.ethClient.Close))
		d.log.Info("Success initialize ethClient")
	}
	return d.ethClient
}

func (d *Deps) BridgeContract() contracts.BridgeContract {
	if d.bridgeContract == nil {
		var err error
		if d.bridgeContract, err = contracts.NewBridgeContract(d.Config().Contracts.Bridge, d.ETHClient()); err != nil {
			d.log.Panic("BridgeContract", zap.Error(err))
		}
		d.log.Info("Success initialize bridge contract")
	}
	return d.bridgeContract
}

func (d *Deps) SwapEventRepository() repositories.SwapEventRepository {
	if d.swapEventRepo == nil {
		d.swapEventRepo = mongo.NewSwapEventRepository(d.MongoClient())
		d.log.Info("Success initialize swapEventRepo")
	}
	return d.swapEventRepo
}

func (d *Deps) BackendSigner() signers.Signer {
	if d.backendSigner == nil {
		var err error
		if d.backendSigner, err = signers.NewBackendSigner(d.Config().Backend.PrivateKey); err != nil {
			d.log.Panic("BackendSigner", zap.Error(err))
		}
		d.log.Info("Success initialize BackendSigner")
	}
	return d.backendSigner
}

func (d *Deps) SwapEventService() services.SwapEventService {
	if d.swapEventService == nil {
		d.swapEventService = services.NewSwapEventService(
			d.ctx,
			d.log,
			d.BridgeContract(),
			d.SwapEventRepository(),
			d.BackendSigner(),
		)
		d.addStopCallback(d.swapEventService.Stop)
		d.log.Info("Success initialize swapEventService")
	}
	return d.swapEventService
}

func (d *Deps) addStopCallback(cb func() error) {
	d.stopCallbacks = append(d.stopCallbacks, cb)
}

func wrapNilError(cb func()) func() error {
	return func() error {
		cb()
		return nil
	}
}

func (d *Deps) WaitForGracefulShutdown() {
	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)
	<-sigChan
	d.log.Info("Catch interrupt signal")
	d.log.Info("Starting graceful shutdown")
	d.cancelFunc()

	for i := len(d.stopCallbacks) - 1; i >= 0; i-- {
		if err := d.stopCallbacks[i](); err != nil {
			d.log.Error("Close", zap.Error(err))
		}
	}

	d.log.Sync()
}
