package main

import "eth-bridge/internal/pkg/dependencies"

func main() {
	deps := dependencies.NewDependencies()
	if err := deps.SwapEventService().ListenSwapEvents(); err != nil {
		panic(err)
	}
	deps.WaitForGracefulShutdown()
}
