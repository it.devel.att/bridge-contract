//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "./Bridge.sol";

contract Token is IERC20MintBurn, ERC20 {
    address private _bridgeAddress;
    address private _owner;

    constructor(string memory _name, string memory _symbol, uint256 _totalSupply) ERC20(_name, _symbol){
        _mint(msg.sender, _totalSupply);
        _owner = msg.sender;
    }

    modifier onlyBridge() {
        require(msg.sender == _bridgeAddress, "Only bridge action");
        _;
    }

    modifier onlyOwner() {
        require(msg.sender == _owner, "Only owner action");
        _;
    }

    function setBridge(address _bridge) external onlyOwner {
        _bridgeAddress = _bridge;
    }

    function mint(address _to, uint256 _amount) external onlyBridge override(IERC20MintBurn) {
        _mint(_to, _amount);
    }

    function burn(address _from, uint256 _amount) external onlyBridge override(IERC20MintBurn) {
        _burn(_from, _amount);
    }
}