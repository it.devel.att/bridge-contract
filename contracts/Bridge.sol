//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

interface IERC20MintBurn is IERC20 {
    function mint(address _to, uint256 _amount) external;

    function burn(address _from, uint256 _amount) external;
}

contract Bridge {
    IERC20MintBurn private _token;
    address private _backendSigner;

    // Sender to nonce mapping
    mapping(address => uint256) private _nonces;
    mapping(address => mapping(uint256 => bool)) private _processedNonces;

    event SwapInitialized(
        address _from,
        address _to,
        uint256 amount,
        uint256 _nonce,
        bytes _signature
    );

    event RedeemComplete(address _from, address _to, uint256 amount);

    constructor(address erc20Token, address backendSigner) {
        _token = IERC20MintBurn(erc20Token);
        _backendSigner = backendSigner;
    }

    function swap(
        address _to,
        uint256 _amount,
        bytes calldata _signature
    ) external {
        require(_amount > 0, "Amount must be greater than 0");
        require(_to != address(0), "Can't send to zero address");
        bytes32 message = prefixed(keccak256(abi.encodePacked(_to, _amount)));
        require(
            recoverSigner(message, _signature) == msg.sender,
            "Sender must sign message with private key"
        );
        _nonces[msg.sender]++;

        _token.burn(msg.sender, _amount);
        emit SwapInitialized(
            msg.sender,
            _to,
            _amount,
            _nonces[msg.sender],
            _signature
        );
    }

    function redeem(
        address _from,
        address _to,
        uint256 _amount,
        uint256 _nonce,
        bytes calldata _senderSignature,
        bytes calldata _backendSignature
    ) external {
        require(
            _processedNonces[_from][_nonce] == false,
            "transfer already processed"
        );
        bytes32 message = prefixed(
            keccak256(
                abi.encodePacked(_from, _to, _amount, _nonce, _senderSignature)
            )
        );
        require(
            recoverSigner(message, _backendSignature) == _backendSigner,
            "wrong backend signature"
        );
        _processedNonces[msg.sender][_nonce] = true;
        _token.mint(_to, _amount);

        emit RedeemComplete(_from, _to, _amount);
    }

    function prefixed(bytes32 hash) internal pure returns (bytes32) {
        return
            keccak256(
                abi.encodePacked("\x19Ethereum Signed Message:\n32", hash)
            );
    }

    function recoverSigner(bytes32 message, bytes memory sig)
        internal
        pure
        returns (address)
    {
        uint8 v;
        bytes32 r;
        bytes32 s;

        (v, r, s) = splitSignature(sig);

        return ecrecover(message, v, r, s);
    }

    function splitSignature(bytes memory sig)
        internal
        pure
        returns (
            uint8,
            bytes32,
            bytes32
        )
    {
        require(sig.length == 65, "wrong signature length");

        bytes32 r;
        bytes32 s;
        uint8 v;

        assembly {
            // first 32 bytes, after the length prefix
            r := mload(add(sig, 32))
            // second 32 bytes
            s := mload(add(sig, 64))
            // final byte (first byte of the next 32 bytes)
            v := byte(0, mload(add(sig, 96)))
        }

        return (v, r, s);
    }
}
