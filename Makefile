BACKEND_CONTRACT_DIR=./backend/internal/app/contracts/generated/
NODE_MODULES=./node_modules


gen-brige-package:
	solc @openzeppelin/=${PWD}/node_modules/@openzeppelin/ --optimize --abi ./contracts/Bridge.sol -o build --overwrite
	solc @openzeppelin/=${PWD}/node_modules/@openzeppelin/ --optimize --bin ./contracts/Bridge.sol -o build --overwrite
	mkdir -p ${BACKEND_CONTRACT_DIR}/bridge/
	abigen --abi=./build/Bridge.abi --bin=./build/Bridge.bin --pkg=bridge --out=${BACKEND_CONTRACT_DIR}bridge/Bridge.go