import { ethers } from "hardhat";
import { utils, BigNumber } from "ethers";

async function main() {
  const initialTokenBalance: BigNumber = utils.parseUnits("100000000", 18);

  const backendAccount = ethers.Wallet.createRandom();

  const Token = await ethers.getContractFactory("Token");
  const erc20Token = await Token.deploy(
    "BridgeToken",
    "BTN",
    initialTokenBalance
  );
  const BridgeContract = await ethers.getContractFactory("Bridge");
  const bridgeContract = await BridgeContract.deploy(
    erc20Token.address,
    backendAccount.address
  );
  await erc20Token.setBridge(bridgeContract.address);

  console.log("Backend private key: ", backendAccount.privateKey);
  console.log("Backend address: ", backendAccount.address);
  console.log("ERC20 address: ", erc20Token.address);
  console.log("Bridge address: ", bridgeContract.address);

  const signer = ethers.provider.getSigner();
  const signerAddress = await signer.getAddress();
  console.log("SignerAddress: ", signerAddress);

  console.log("Balance fo signer:", await erc20Token.balanceOf(signerAddress));
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
