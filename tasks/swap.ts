import { task } from "hardhat/config";
import { utils, BigNumber } from "ethers";

async function getContract(hre: any, contractAddress: string) {
    const Token = await hre.ethers.getContractFactory("Bridge");
    const contract = await Token.attach(contractAddress);
    return contract;
}


function genMessage(ethers: any, addr: string, amount: BigNumber): string {
    return ethers.utils.solidityKeccak256(
        ["address", "uint256"],
        [addr, amount],
    )
}

task("swap", "Swap coins")
    .addParam("contract", "Address of Bridge contract")
    .addParam("to", "Address to who transfer tokens")
    .addParam("amount", "Amount of tokens")
    .setAction(async (taskArgs, hre) => {
        const bridge = await getContract(hre, taskArgs.contract);
        const amount = utils.parseUnits(taskArgs.amount, 18);
        const to = taskArgs.to;
        const message = genMessage(hre.ethers, to, amount);

        const signers = await hre.ethers.getSigners();
        const signer = signers[0];

        const signature = await signer.signMessage(hre.ethers.utils.arrayify(message));

        await bridge.swap(to, amount, signature);
        console.log("Succes send tokens for swap")
    });

task("redeem", "Swap coins")
    .addParam("contract", "Address of Bridge contract")
    .addParam("from", "Address from")
    .addParam("to", "Address to")
    .addParam("amount", "Amount")
    .addParam("nonce", "Nonce")
    .addParam("signature", "Args signature")
    .addParam("backendsignature", "Backend signature")
    .setAction(async (taskArgs, hre) => {
        console.log(taskArgs);
        const bridge = await getContract(hre, taskArgs.contract);

        await bridge.redeem(taskArgs.from, taskArgs.to, taskArgs.amount, taskArgs.nonce, taskArgs.signature, taskArgs.backendsignature)
    });